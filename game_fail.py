from math import sin, cos
import pygame, sys
size = width, height = 320, 240
black = 0, 0, 0
def main():
    pygame.init()
    screen = pygame.display.set_mode(size)
    gameover = False
    ball = pygame.image.load("basketball.png")
    ballrect = ball.get_rect()
    r = 50
    angle = 0
    while not gameover:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameover = True
        ballrect.x = width // 2 -ballrect.width // 2 + r * sin(angle)
        ballrect.y = height // 2 -ballrect.height // 2 + r * cos(angle)
        angle += 0.04
        screen.fill(black)
        screen.blit(ball, ballrect)
        pygame.display.flip()
        pygame.time.wait(10)
    sys.exit()
main()